﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace linq
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            string choice = "";
            do {
                Console.WriteLine("Что вы хотите сделать? (Введите q для выхода)");
                Console.WriteLine("1. Получить количество тасков в проектах конкретного пользователя");
                Console.WriteLine("2. Получить список тасков конкретного пользователя, где название таска меньше 45 символов");
                Console.WriteLine("3. Получить список выполненных в текущем году тасков конкретного пользователя");
                Console.WriteLine("4. Получить список команд, участники которых старше 10 лет");
                Console.WriteLine("5. Получить список пользователей с их тасками");
                Console.WriteLine("6. Получить следующую структуру:\n - Пользователь\n - Последний проект пользователя\n - Общее кол-во тасков под последним проектом\n - Общее кол-во незавершенных или отмененных тасков для пользователя\n - Самый долгий таск пользователя");
                Console.WriteLine("7. Получить следующую структуру:\n - Проект\n - Самый длинный таск проекта \n - Самый короткий таск проекта\n - Общеее кол-во пользователей в команде проекта");
                choice = Console.ReadLine();
                switch(choice)
                {
                    case "1": 
                        var result1 = Services.TasksAmountForUsersProjects(userId());
                        foreach(var item in result1) {
                            Console.WriteLine($"Project {item.Key.Name} has {item.Value} tasks.");
                        }
                        wait();
                        break;
                    case "2":
                        var result2 = Services.UsersTasks(userId());
                        foreach(var item in result2) {
                            Console.WriteLine($"- {item.Id} {item.Name}");
                        }
                        wait();
                        break;
                    case "3":
                        var result3 = Services.FinishedThisYearTasksForUser(userId());
                        foreach(var item in result3) {
                            Console.WriteLine($"- {item.Item1} {item.Item2}");
                        }
                        wait();
                        break;
                    case "4":
                        var result4 = Services.TeamsWithUsersOlder10();
                        foreach(var item in result4) {
                            Console.WriteLine($"{item.Item1} {item.Item2}");
                            foreach(var user in item.Item3){
                                Console.WriteLine($" - {user.FirstName}. Дата рождения: {user.Birthday}. Дата регистрации: {user.RegisteredAt}");
                            }
                        }
                        wait();
                        break;
                    case "5":
                        var result5 = Services.AllUsersWithTasks();
                        foreach(var item in result5) {
                            Console.WriteLine($"{item.Item1.Id} {item.Item1.FirstName} {item.Item1.LastName}:");
                            foreach (var task in item.Item2) {
                                Console.WriteLine($" - {task.Id} {task.Name}");
                            }
                        }
                        wait();
                        break;
                    case "6":
                        var result6 = Services.GetUserDetails(userId());
                            Console.WriteLine($"Пользователь: {result6.User.FirstName}\nПоследний проект пользователя: {result6.LastProject.Name}\nКоличество тасков в последнем проекте: {result6.ProjectsTasks}\nКоличество незавершенных тасков пользователя: {result6.NotFinishedTasks}\nСамый долгий таск пользователя: {result6.LongestTask.Name}");
                        wait();
                        break;
                    case "7":
                        var result7 = Services.GetProjectsDetails();
                        foreach(var item in result7) {
                            Console.WriteLine($"Проект: {item.Project.Name}\nСамый длинный таск проекта (по описанию): {item.LongestTask.Name}\nСамый короткий таск проекта (по имени): {item.ShortestTask.Name}\nОбщее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3: {item.Members}\n");
                        }
                        wait();
                        break;
                }
            } while (choice != "q");
        }

        static int userId() {
            int id = -1;
                while (id < 0 || id > 50){
                    Console.WriteLine("Введите ID пользователя: ");
                    int.TryParse(Console.ReadLine(), out id);
                }
            return id;
        }

        static void wait(){
            Console.WriteLine("\nPress Enter");
            Console.ReadLine();
        }
    }
}
