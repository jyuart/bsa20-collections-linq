public class ProjectDetails {
    public ProjectDetails(Project _project, Task _longestTask, Task _shortestTask, int _members) {
        Project = _project;
        LongestTask = _longestTask;
        ShortestTask = _shortestTask;
        Members = _members;
    }
    public Project Project {get; set;}
    public Task LongestTask {get; set;}
    public Task ShortestTask {get; set;}
    public int Members {get; set;}
}