public class UserDetails {
    public UserDetails(User _user, Project _lastProject, int _projectsTasks, int _notFinishedTasks, Task _longestTask) {
        this.User = _user;
        this.LastProject = _lastProject;
        this.ProjectsTasks = _projectsTasks;
        this.NotFinishedTasks = _notFinishedTasks;
        this.LongestTask = _longestTask;
    }
    public User User {get; set;}
    public Project LastProject {get; set;}
    public int ProjectsTasks {get; set;}
    public int NotFinishedTasks {get; set;}
    public Task LongestTask {get; set;}
}