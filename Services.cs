using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;



public static class Services {

    // 1. Получить количество тасков у проекта конкретного пользователя
    public static Dictionary<Project,int> TasksAmountForUsersProjects(int id) {
        var user = Data.GetOneUser(id).Result;
        var tasks = Data.GetTasks().Result;
        var projects = Data.GetProjects().Result;
        var result = (from project in projects.Where(x => x.AuthorId == user.Id)
                    join task in tasks on project.Id equals task.ProjectId into tasksInProject
                    select (project, tasksInProject.Count())).ToDictionary(key => key.project, value => value.Item2);
        return result;
    }

    // 2. Получить список тасков, назначенных на конкретного пользователя, где name таска < 45 символов
    public static IEnumerable<Task> UsersTasks(int id) {
        var tasks = Data.GetTasks().Result;
        var users = Data.GetUsers().Result;
        var result = from task in tasks
                    where task.PerformerId == id
                    where task.Name.Length < 45
                    select task;

        return result;       
    }

    // 3. Получить список из коллекции тасков, которые выполнены (finished) в текущем (2020) году для конкретного пользователя
    public static IEnumerable<(int, string)> FinishedThisYearTasksForUser(int id) {
        var tasks = Data.GetTasks().Result;
        var result = from task in tasks
                    where task.State == 2
                    where task.FinishedAt.Year == 2020
                    where task.PerformerId == id
                    select (task.Id, task.Name);
        return result;
    }

    // 4. Получить список из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
    public static IEnumerable<(int, string, List<User>)> TeamsWithUsersOlder10() {
        var teams = Data.GetTeams().Result;
        var users = Data.GetUsers().Result;
    var result = from team in teams
                join user in users on team.Id equals user.TeamId into usersInTeam
                where usersInTeam.Any()
                where usersInTeam.All(x=>x.Birthday.Year < 2010)
                select(team.Id, team.Name, usersInTeam.OrderByDescending(x=>x.RegisteredAt).ToList());
        return result;
    }

    // 5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
    public static IEnumerable<(User, List<Task>)> AllUsersWithTasks(){
        var tasks = Data.GetTasks().Result;
        var users = Data.GetUsers().Result;
        var result = from user in users
                    join task in tasks on user.Id equals task.PerformerId into usersTasks
                    orderby user.FirstName
                    where usersTasks.Any()
                    select(user, usersTasks.OrderByDescending(x => x.Name.Length).ToList());
        return result;
    }

    // 6. Получить структуру с подробностями о пользователе
    public static UserDetails GetUserDetails(int id) {
        var user = Data.GetOneUser(id).Result;
        var tasks = Data.GetTasks().Result;
        var projects = Data.GetProjects().Result;
        var result = from project in projects
                    join task in tasks on project.Id equals task.ProjectId
                    select new UserDetails(user, 
                    projects.Where(x => x.AuthorId == user.Id).Aggregate((x, y) => x.CreatedAt > y.CreatedAt ? x: y), 
                    (tasks.Where(x => x.ProjectId == (projects.Where(x=> x.AuthorId == user.Id).Aggregate((x, y) => x.CreatedAt > y.CreatedAt ? x: y)).Id)).Count(),
                    tasks.Where(x => x.PerformerId == user.Id && x.State != 2).Count(),
                    tasks.Where(x => x.PerformerId == user.Id).Aggregate((x, y) => (x.FinishedAt - x.CreatedAt) > (y.FinishedAt - y.CreatedAt) ? x : y));
        return result.First();
    }
                    
    // 7. Получить структуру с подробностями о проекте
    public static List<ProjectDetails> GetProjectsDetails() {
        var users = Data.GetUsers().Result;
        var projects = Data.GetProjects().Result;
        var tasks = Data.GetTasks().Result;
        var teams = Data.GetTeams().Result;
        var result = (from project in projects
                    join task in tasks on project.Id equals task.ProjectId into tasksInProject
                    join team in teams on project.TeamId equals team.Id
                    join user in users on team.Id equals user.TeamId into someUsers
                    where tasksInProject.Any()
                    select new ProjectDetails(project,
                     tasksInProject.Aggregate((x, y) => x.Description.Length > y.Description.Length ? x: y),
                     tasksInProject.Aggregate((x, y) => x.Name.Length < y.Name.Length ? x : y),
                     someUsers.Where(x => x.TeamId == project.TeamId && project.Description.Length > 20 || tasksInProject.Count() < 3).Count())).ToList();
        return result;
    }
}