using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

public static class Data {
    public static readonly HttpClient client = new HttpClient();
    public static async Task<List<Project>> GetProjects() {
        HttpResponseMessage projectsResponse = await client.GetAsync("https://bsa20.azurewebsites.net/api/projects");
        var projectsContent = projectsResponse.Content.ReadAsStringAsync();
        var projects = JsonConvert.DeserializeObject<List<Project>>(projectsContent.Result);
        return projects;
    }

    public static async Task<List<Task>> GetTasks() {
        HttpResponseMessage tasksResponse = await client.GetAsync("https://bsa20.azurewebsites.net/api/tasks");
        var tasksContent = tasksResponse.Content.ReadAsStringAsync();
        var tasks = JsonConvert.DeserializeObject<List<Task>>(tasksContent.Result);
        return tasks;
    }

    public static async Task<List<User>> GetUsers() {
        HttpResponseMessage usersResponse = await client.GetAsync("https://bsa20.azurewebsites.net/api/users");
        var usersContent = usersResponse.Content.ReadAsStringAsync();
        var users = JsonConvert.DeserializeObject<List<User>>(usersContent.Result);
        return users;
    }

        public static async Task<User> GetOneUser(int id) {
        HttpResponseMessage userResponse = await client.GetAsync($"https://bsa20.azurewebsites.net/api/users/{id}");
        var userContent = userResponse.Content.ReadAsStringAsync();
        var user = JsonConvert.DeserializeObject<User>(userContent.Result);
        return user;
    }

    public static async Task<List<Team>> GetTeams() {
        HttpResponseMessage teamsResponse = await client.GetAsync("https://bsa20.azurewebsites.net/api/teams");
        var teamsContent = teamsResponse.Content.ReadAsStringAsync();
        var teams = JsonConvert.DeserializeObject<List<Team>>(teamsContent.Result);
        return teams;
    }
}